package com.tsystems.javaschool.tasks.calculator;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.LinkedList;
import java.util.Locale;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.length() == 0) {
            return null;
        }

        LinkedList<Double> numbers = new LinkedList<>();
        LinkedList<Character> operators = new LinkedList<>();
        try {
            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (isDelim(c))
                    continue;

                if (c == '(')
                    operators.add('(');
                else if (c == ')') {
                    while (operators.getLast() != '(')
                        processOperator(numbers, operators.removeLast());
                    operators.removeLast();
                } else if ((operators.isEmpty() || operators.getLast() == '(') && c == '-' && numbers.isEmpty()) {
                    numbers.addFirst(0d);
                    operators.add('-');
                } else if ((operators.isEmpty() || operators.getLast() == '(') && c == '+' && numbers.isEmpty()) {
                    numbers.addFirst(0d);
                    operators.add('+');
                } else if (isOperator(c)) {
                    while (!operators.isEmpty() && priority(operators.getLast()) >= priority(c)) {
                        processOperator(numbers, operators.removeLast());
                    }
                    operators.add(c);
                } else {
                    StringBuilder stringBuilder = new StringBuilder();
                    while (i < statement.length() && (Character.isDigit(statement.charAt(i)) || statement.charAt(i) == '.')) {
                        stringBuilder.append(statement.charAt(i++));
                    }
                    --i;
                    numbers.add(Double.parseDouble(stringBuilder.toString()));
                }
            }
            while (!operators.isEmpty()) {
                processOperator(numbers, operators.removeLast());
            }
            double result = numbers.get(0);

            if (Double.isInfinite(result)) {
                return null;
            }

            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.getDefault());
            otherSymbols.setDecimalSeparator('.');
            DecimalFormat decimalFormat = new DecimalFormat("#.####", otherSymbols);
            return decimalFormat.format(result);
        } catch (RuntimeException e) {
            return null;
        }
    }

    private static boolean isDelim(char c) {
        return c == ' ';
    }

    private static boolean isOperator(char c) {
        return c == '+' || c == '-' || c == '*' || c == '/';
    }

    private static int priority(char operators) {
        switch (operators) {
            case '+':
                return 1;
            case '-':
                return 1;
            case '*':
                return 2;
            case '/':
                return 2;
            default:
                return -1;
        }
    }

    private static void processOperator(LinkedList<Double> numbers, char operators) {
        double r = numbers.removeLast();
        double l = numbers.removeLast();
        switch (operators) {
            case '+':
                numbers.add(l + r);
                break;
            case '-':
                numbers.add(l - r);
                break;
            case '*':
                numbers.add(l * r);
                break;
            case '/':
                numbers.add(l / r);
                break;
        }
    }

}
