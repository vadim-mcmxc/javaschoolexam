package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int n = inputNumbers.size();
        if (!inputNumbers.contains(null) && (Math.sqrt(8 * n + 1) - 1) % 2 == 0) { //checking that it's triangle value
            Collections.sort(inputNumbers);
            int a = (int) ((Math.sqrt(8 * n + 1) - 1) / 2); //amounts of strings
            int b = (int) (((Math.sqrt(8 * n + 1) - 1) / 2) + ((Math.sqrt(8 * (n - 1) + 1) - 1) / 2)); //amount of columns
            int[][] array = new int[a][b];
            int c = 0;
            for (int i = 0; i < a; i++) {
                for (int j = i; j >= 0; j--) {
                    array[i][(b - 1) / 2 + i - 2 * j] = inputNumbers.get(c++);
                }
            }
            return array;

        } else {
            throw new CannotBuildPyramidException();
        }
    }

}
