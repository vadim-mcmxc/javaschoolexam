package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        int startPosition = 0;
        boolean result = false;
        if (x != null && y != null) {
            if(x.isEmpty()) {
                result = true;
            } else {
                for (int i = 0; i < x.size(); i++) {
                    boolean included = false;
                    for (int j = startPosition; j < y.size(); j++) {
                        if (x.get(i).equals(y.get(j))) {
                            included = true;
                            startPosition = j;
                        }
                    }
                    result = included;
                }
            }

        } else {
            throw new IllegalArgumentException();
        }

        return result;
    }
}
